import React, { useState, useEffect } from "react";
import axios from "axios";
import "./App.css";

function App() {
  const [citiesData, setCitiesData] = useState([]);

  useEffect(() => {
    const fetchData = async () => {
      try {
        const response = await axios.get("http://127.0.0.1:4000/api/cities");
        setCitiesData(response.data);
      } catch (error) {
        console.error("Error fetching city data:", error);
      }
    };
    fetchData();
  }, []);
  return (
    <>
      <table className="custom-table">
        <thead>
          <tr>
            <th>Code Insee</th>
            <th>City</th>
            <th>Population</th>
          </tr>
        </thead>
        <tbody>
          {citiesData.map((city) => (
            <tr key={city.insee}>
              <td>{city.insee}</td>
              <td>{city.name}</td>
              <td>{city.population}</td>
            </tr>
          ))}
        </tbody>
      </table>
    </>
  );
}

export default App;
