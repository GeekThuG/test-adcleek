const db = require("./database");
const express = require("express");
const cors = require("cors");
const axios = require("axios");
require("dotenv").config();
const PORT = 4000;

// config express
const app = express();
const routes = express.Router();
app.use("/api", routes);

// a ajouter pour la communication entre le front et le back en dev lorsque express est instancié
app.use(
  cors({
    origin: "http://127.0.0.1:3000",
    methods: "GET,HEAD,PUT,PATCH,POST,DELETE",
    credentials: true,
    optionsSuccessStatus: 204,
  })
);

// Initialisation de la base avec les deux tables nécessaires (à garder)
db.init();

// API to display cities
routes.get("/cities", async (req, res) => {
  try {
    const cities = await db.all("SELECT * FROM city");

    res.header("Access-Control-Allow-Origin", "http://localhost:3000");
    res.header(
      "Access-Control-Allow-Methods",
      "GET,HEAD,PUT,PATCH,POST,DELETE"
    );
    res.header(
      "Access-Control-Allow-Headers",
      "Origin, X-Requested-With, Content-Type, Accept"
    );

    res.json(cities);
  } catch (error) {
    console.error("Error fetching city information:", error);
    res.status(500).json({ error: "Internal Server Error" });
  }
});

// API to display forecast
routes.get("/cities/:inseeCode/forecast", async (req, res) => {
  const inseeCode = req.params.inseeCode;
  try {
    // Vérif if cities exist
    const city = await db.get(
      `SELECT * FROM city WHERE insee = "${inseeCode}"`
    );
    if (!city) {
      // If not return 404
      return res.status(404).json({ error: "City not found" });
    }
    // Get forecast from external API
    const response = await axios.get(
      `${process.env.METEO_CONCEPT_API}?token=${process.env.METEO_CONCEPT_TOKEN}&insee=${inseeCode}`
    );
    // Return data in JSON file
    res.json({ city, forecasts: response.data });
  } catch (error) {
    console.error("Error fetching forecast information:", error);
    res.status(500).json({ error: "Internal Server Error" });
  }
});

// Start server
app.listen(PORT, () => {
  console.log(`Server up and running on http://localhost:${PORT}`);
});
